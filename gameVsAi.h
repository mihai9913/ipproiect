#include <time.h>

#define MAX 10
#define FUNDAL CYAN
#define PatratAI 2
using namespace std;



// MOTHER OF THE FILL ALGORITHM

struct PozitiiAI
{
    int x;
    int y;
    int NrVecini;
    int AreVecinPatr;
    int AjungeBila;

}PozAI[64];

int NrPozAI;
void drawPlayersAI()
{
    setfillstyle(SOLID_FILL, FUNDAL);
    bar(0, 70, stanga, getwindowheight()-200);

    setfillstyle(SOLID_FILL, Playercolor[1]);
    bar(90, 210, 110, 230);
        setfillstyle(SOLID_FILL, Playercolor[2]);
    bar(90, 310, 110,330);
    if(activePlayer==1)
    {
        setcolor(BLACK);
        settextstyle(GOTHIC_FONT, HORIZ_DIR, 4);
        outtextxy(100, 200, playersName[1]);

        settextstyle(GOTHIC_FONT, HORIZ_DIR, 2);
        outtextxy(100, 300, playersName[2]);

    }
    if(activePlayer==2)
    {
        setcolor(BLACK);
        settextstyle(GOTHIC_FONT, HORIZ_DIR, 2);
        outtextxy(100, 200, playersName[1]);

        settextstyle(GOTHIC_FONT, HORIZ_DIR, 4);
        outtextxy(100, 300, playersName[2]);

    }
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 2);

}
int Verificare (int l, int c)
{
    return (l>=1 && l<=8 && c>=1 && c<=8);
}

int colorflag2=-1;
int v3[10][10];
int VculoriOC2[20];
int gasesteBila(int i, int j)
{   v3[i][j]=activePlayer;

    for(int k=0;k<=3;k++)
    {
        int ll = i+d1[k];
        int cc = j+d2[k];
        if(TablaDeJoc[ll][cc]>=100 && TablaDeJoc[ll][cc]<200 && VculoriOC2[TablaDeJoc[ll][cc]-100]==0 && colorflag2==-1)
        {
            VculoriOC2[TablaDeJoc[ll][cc]-100]=1;
            colorflag2 = TablaDeJoc[ll][cc]-100;
        }

        else if((TablaDeJoc[ll][cc]==2 ) && v3[ll][cc]==0)
           gasesteBila(ll, cc);

    }

}

   void gandesteAI(int &linieAI, int &coloanaAI)
{
    // 100+culoare  cerc
    // 200+culoare linie completa
    // 1-patrat pus de player
    // 2-patrat pus de ai

    int d1[]= {0,1,0,-1};
    int d2[]= {1,0,-1,-0};
    int ok=1;

    NrPozAI=0;
    for(int i=1; i<=8; i++)
        for(int j=1; j<=8; j++)
            if(TablaDeJoc[i][j]==0)
            {
                ok=0;
                PozAI[++NrPozAI].x=i;
                PozAI[NrPozAI].y=j;

                for(int k=0; k<=3; k++)
                {
                   {
                    if((TablaDeJoc[PozAI[NrPozAI].x+d1[k]][PozAI[NrPozAI].y+d2[k]]==0 || TablaDeJoc[PozAI[NrPozAI].x+d1[k]][PozAI[NrPozAI].y+d2[k]]==2 ||
                       TablaDeJoc[PozAI[NrPozAI].x+d1[k]][PozAI[NrPozAI].y+d2[k]]>100 && TablaDeJoc[PozAI[NrPozAI].x+d1[k]][PozAI[NrPozAI].y+d2[k]]<150) && Verificare(PozAI[NrPozAI].x+d1[k], PozAI[NrPozAI].y+d2[k]))
                      PozAI[NrPozAI].NrVecini++;

                    if(TablaDeJoc[PozAI[NrPozAI].x+d1[k]][PozAI[NrPozAI].y+d2[k]]==2  && Verificare(PozAI[NrPozAI].x+d1[k], PozAI[NrPozAI].y+d2[k]))
                           PozAI[NrPozAI].AreVecinPatr++;

                   }
                        cl(v3);
                        colorflag2=-1;
                        gasesteBila(PozAI[NrPozAI].x,PozAI[NrPozAI].y);
                        PozAI[NrPozAI].AjungeBila=colorflag;
                        cout<<PozAI[NrPozAI].AjungeBila;

                }

            }

        for(int i=1;i<NrPozAI;i++)
                for(int j=i+1;j<=NrPozAI;j++)
                    if(PozAI[i].NrVecini<PozAI[j].NrVecini)
                    { swap(PozAI[i].NrVecini,PozAI[j].NrVecini);
                      swap(PozAI[i].AjungeBila,PozAI[j].AjungeBila);
                      swap(PozAI[i].AreVecinPatr,PozAI[j].AreVecinPatr);
                      swap(PozAI[i].x,PozAI[j].x);
                      swap(PozAI[i].y,PozAI[j].y);
                    }
        int cercCareAjungeLaBila=-1;
        int cercCuCeiMaiMultiVeciniLiberi=-1;
        int areVecinPtr=-1;
        for(int i=1;i<=NrPozAI && cercCareAjungeLaBila==-1 && areVecinPtr==-1; i++)
        {
            if(PozAI[i].AjungeBila!=0 && PozAI[i].AreVecinPatr!=0)
               {
                 areVecinPtr=PozAI[i].AreVecinPatr;
                 cercCareAjungeLaBila=i;
                 linieAI=PozAI[i].x;
                 coloanaAI=PozAI[i].y;

               }

        }
        if(cercCareAjungeLaBila==0 &&areVecinPtr==0)
        {
            for(int i=1;i<=NrPozAI && cercCareAjungeLaBila==-1;i++)
            {
            if(PozAI[i].AjungeBila!=0)
               {

                 cercCareAjungeLaBila=i;
                 linieAI=PozAI[i].x;
                 coloanaAI=PozAI[i].y;

               }

            }
        }

        if(cercCareAjungeLaBila==-1)
               {
                   cercCuCeiMaiMultiVeciniLiberi==1;
                    linieAI=PozAI[1].x;
                    coloanaAI=PozAI[1].y;
               }


        }






void punePiesaAi()
{
        int x,y;
    int x1, y1, x2, y2;
    int xmijloc, ymijloc;

        int AIlinie, AIcoloana;
        // aici vine code pt ai
                activePlayer=2;
                gandesteAI(AIlinie, AIcoloana);
                cout<<endl<<endl<<AIlinie<<" "<<AIcoloana<<endl;
                setcolor(FUNDAL);
                setfillstyle(SOLID_FILL,FUNDAL);
                x1=stanga+latura*(AIcoloana-1);
                y1=sus+latura*(AIlinie-1);
                x2=x1+latura;
                y2=y1+latura;
                xmijloc=(x1+x2)/2;
                ymijloc=(y1+y2)/2;
                int ok=1;
                for(int i=1;i<=8;i++)
                {

                    if(cercuri[i].x==xmijloc&&cercuri[i].y==ymijloc)
                    {
                        ok=0;
                    }
                }
                // afisez patrat
                if(ok==1){
                setcolor(Playercolor[activePlayer]);
                setfillstyle(SOLID_FILL,Playercolor[activePlayer]);
                bar(x1+10,y1+10,x2-10,y2-10);
                TablaDeJoc[AIlinie][AIcoloana]=activePlayer;
                cout<<"fdsf";
                int aa = fil(AIlinie, AIcoloana);
                for(int i=1;i<=8;i++)
                {
                    for(int j=1;j<=8;j++)
                        cout<<TablaDeJoc[i][j]<<" ";
                    cout<<endl;
                }
                cout<<"AI aa: "<<aa<<endl;
                if(aa>=6)
                    {
                        cl(v);
                        colorflag=-1;
                        filFindColor(AIlinie, AIcoloana);
                        cout<<"colorFind: "<<colorflag<<endl;
                        if(colorflag!=-1)
                        {
                            //aici e bug u ???
                            cl(v2);
                            filRe=7;

                            fillColor(AIlinie, AIcoloana, colorflag);
                            playersLines[activePlayer]++;

                        }
                    }


                }

        activePlayer=1;
}

int punerePiesaAI()
{
    int linia,coloana,x,y;
    int x1, y1, x2, y2;
    int xmijloc, ymijloc;
    if(ismouseclick(WM_LBUTTONDOWN))
    {
        clearmouseclick(WM_LBUTTONDOWN);
        x=mousex();
        y=mousey();
        if (!(x>=stanga && x<=stanga+width && y>=sus&&y<=sus+height))
            gata=true;
        else
        {
            linia=(y-sus)/latura+1;
            coloana=(x-stanga)/latura+1;
            if (TablaDeJoc[linia][coloana]==0)
            {

                setcolor(FUNDAL);
                setfillstyle(SOLID_FILL,FUNDAL);
                x1=stanga+latura*(coloana-1);
                y1=sus+latura*(linia-1);
                x2=x1+latura;
                y2=y1+latura;
                xmijloc=(x1+x2)/2;
                ymijloc=(y1+y2)/2;
                int ok=1;
                for(int i=1;i<=8;i++)
                {

                    if(cercuri[i].x==xmijloc&&cercuri[i].y==ymijloc)
                    {
                        ok=0;
                    }
                }
                // afisez patrat
                if(ok==1){
                setcolor(Playercolor[activePlayer]);
                setfillstyle(SOLID_FILL,Playercolor[activePlayer]);
                bar(x1+10,y1+10,x2-10,y2-10);
                TablaDeJoc[linia][coloana]=activePlayer;

                cl(v);
                int aa = fil(linia, coloana);

                if(aa>=6)
                    {
                        cl(v);
                        colorflag=-1;
                        filFindColor(linia, coloana);
                        if(colorflag!=-1)
                        {
                            cl(v2);
                            filRe=7;
                            fillColor(linia, coloana, colorflag);
                            playersLines[activePlayer]++;

                        }
                    }


                    activePlayer=2;

                       // drawPlayersAI();
                        nrc++;
                        if(nrc+8==64 || (nrc>=10 && earlyEnd()==0))
                            return 1;
                            punePiesaAi();
                }
            }
        }
    }

    return 2;
}


int PvAI ()
{
    settextjustify(CENTER_TEXT, CENTER_TEXT);
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 4);
    setcolor(BLACK);

    outtextxy(getwindowwidth()/2, getwindowheight()/10-20, displayText[displayLanguage][32]);

    settextstyle(GOTHIC_FONT, HORIZ_DIR, 1);
    outtextxy(getwindowwidth()/2, getwindowheight()-60, displayText[displayLanguage][33]);
    outtextxy(getwindowwidth()/2, getwindowheight()-40, displayText[displayLanguage][34]);
    activePlayer=1;
    playersScore[1]=playersScore[2]=0;
    nrc=0;

    for(int i=0;i<20;i++)
        VculoriOC[i]=0;

    for(int i=0;i<10;i++)
        playersLines[i]=0;

    gameOn=1;

    drawPlayersAI();


    while(gameOn==1)
    {
     //cout<<punerePiesa();
        //if(
         if(punerePiesaAI()==1)
            return 1;
           //gameOn=1;
           // cout<<"dsad";
        if(kbhit())
        {
            char c;
            c = getch();
            if(c==27) return -1;
        }
    }
}

void desenAI()
{
    int k=0;
    int i,j;
    numar=8;
    width=std::min(getwindowheight(),getwindowwidth())*3/4;
    latura=width/numar;
    height=width;
    sus=(getwindowheight()-width)/2;
    stanga=(getwindowwidth()- width)/2;
    setbkcolor(FUNDAL);
    clearviewport();
    setcolor(BLUE);

    for (i=1; i<=numar; i++)
        for (j=1; j<=numar; j++)
        {
            rectangle(stanga+latura*(i-1),sus+latura*(j-1),stanga+latura*i,sus+latura*j);
            k++;
            CentreCasute[k].x=float((stanga+latura*(i-1)+stanga+latura*i)/2);
            CentreCasute[k].y=float((sus+latura*(j-1)+sus+latura*j)/2);
        }
    PunereCercuriRandom();

}
