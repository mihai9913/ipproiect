#include <iostream>
#include <winbgim.h>
#include <graphics.h>
#include <cstring>
#include <windows.h>
#include <mmsystem.h>
#include <thread>
#include <time.h>
#include <iomanip>

#include "timefunctions.h"
#include "gameVariables.h"
#include "playerManager.h"
#include "sound.h"
#include "menu.h"

#include "gameVSplayer.h"
#include "gameVSAi.h"
#include "autoFill.h"


using namespace std;



void gameVSPlayer()
{

    manele();

    desen();

    cl(v);
    cl(v2);

    int gata=false;
    do
    {
        gata = PvP();
        cout<<gata;
    }
    while (!gata);
    if(gata!=-1)
    {addHS();
    endGame();
    }
}
void gameVSAI()
{
  for(int i=1;i<=8;i++)
        for(int j=1;j<=8;j++)
            TablaDeJoc[i][j]=0;
    manele();
    desenAI();

    cl(v);
    cl(v2);

    int gata=false;
    do
    {
        gata = PvAI();
        cout<<gata;
    }
    while (!gata);
    if(gata!=-1)
    {
    if(playersLines[1]>playersLines[2])
        {
            playersScore[1]=10;
            playersScore[2]=0;
        }
    else{
            playersScore[1]=0;
            playersScore[2]=10;
        }
    endGame();
    }
}
void gameAutoFill()
{

    manele();
    desenAutoFill();
    int a = autoFill();
    if( a == 1)
        getch();

}

int main()
{


    initDispText();
    int x = GetSystemMetrics(SM_CXSCREEN);
    int y = GetSystemMetrics(SM_CYSCREEN);
    resolutionsHeights[4]=y;
    resolutionsWidths[4]=x;
    initwindow(resolutionsWidths[currentResolutionInd], resolutionsHeights[currentResolutionInd]);
    initMenu();
    int opt=100;

    while(opt!=-1)
    {

    opt = drawMenu();
    if(opt==1)
    {
        getPlayersName();
        gameVSPlayer();


    }
    if(opt==3)
    {

        getPlayerName(1);
        strcpy(playersName[2], "calculator");
        gameVSAI();


    }
    if(opt==2)
    {

        gameAutoFill();


    }
    else if(opt==-1)
    {
        delay(300);
            return 0;
    }
    }
    //getch();
    return 0;
}
