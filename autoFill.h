#include <time.h>

#define MAX 10
#define FUNDAL CYAN
using namespace std;

int stangaAutoFill,susAutoFill,widthAutoFill,heightAutoFill,laturaAutoFill, numarAutoFill;


int tablaAutoFill[MAX][MAX];


int foundSol=0;

struct cercAutoFill
{
    float x;
    float y;


} cercuriAutoFill [10];

int nrcercuriAutoFill;

int n=8,a[10][10];
int dl[]={0,1,-1,0,0};
int dc[]={0,0,0,1,-1};
struct pozitie {int l,c;}q[10];

void drawBottomText()
{
    setfillstyle(SOLID_FILL, FUNDAL);
    setcolor(BLACK);
    bar(0, getwindowheight()-80, getwindowwidth(), getwindowheight());
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 1);
    char txt[255], txt2[255];
    strcpy(txt, displayText[displayLanguage][25]);
    strcpy(txt2, displayText[displayLanguage][26]);

    txt[6] = 8-nrcercuriAutoFill+'0';
    if(nrcercuriAutoFill==8)
    {
     //   strcpy(txt, NULL);
        if(foundSol==1)
        {
            strcpy(txt, displayText[displayLanguage][27]);
            strcpy(txt2, displayText[displayLanguage][28]);
        }
        else
        {
            strcpy(txt, displayText[displayLanguage][29]);
            strcpy(txt2, displayText[displayLanguage][30]);
        }

    }
    outtextxy(getwindowwidth()/2, getwindowheight()-60, txt);
    outtextxy(getwindowwidth()/2, getwindowheight()-40, txt2);
}

void cpyToTabla()
{
    for(int i=1;i<=n;i++)
       for(int j=1;j<=n;j++)
            tablaAutoFill[i][j]=a[i][j];
}

int verif(int inou,int jnou)
{
    if(inou<1 or inou>n or jnou<1 or jnou>n) return 0;
    if(a[inou][jnou]!=0) return 0;
    return 1;
}

void drawSquares()
{

    for (int i=1; i<=numarAutoFill; i++)
        for (int j=1; j<=numarAutoFill; j++)
        {
            setfillstyle(SOLID_FILL, Vculori[tablaAutoFill[j][i]]);

            bool ok=1;
            for(int k=1;k<=nrcercuriAutoFill;k++)
                if(i==q[k].c && j==q[k].l)
                    ok=0;

            if(ok && tablaAutoFill[j][i]!=0)
                bar(stangaAutoFill+laturaAutoFill*(i-1)+10,susAutoFill+laturaAutoFill*(j-1)+10,stangaAutoFill+laturaAutoFill*i-10,susAutoFill+laturaAutoFill*j-10);
        }


}



void back(int i,int j,int k,int pas)
{
    if(!foundSol)
    for(int d=1;d<=4;d++)
    {
        int inou=i+dl[d];
        int jnou=j+dc[d];
        if(verif(inou,jnou))
        {
            a[inou][jnou]=k;
            if(pas==n)
                if(k<n)
                    back(q[k+1].l,q[k+1].c,k+1,2);
                else
                {
                    cpyToTabla();
                    foundSol=1;
                    drawBottomText();
                }
            else
                back(inou,jnou,k,pas+1);
            a[inou][jnou]=0;
        }
    }
}

void punereCerc()
{
    int linia,coloana,x,y;
    int x1, y1, x2, y2;
    int xmijloc, ymijloc;
    if(ismouseclick(WM_LBUTTONDOWN))
    {
        clearmouseclick(WM_LBUTTONDOWN);
        x=mousex();
        y=mousey();
        if (!(x>=stangaAutoFill && x<=stangaAutoFill+widthAutoFill && y>=susAutoFill&&y<=susAutoFill+heightAutoFill))
            gata=true;
        else
        {
            linia=(y-susAutoFill)/laturaAutoFill+1;
            coloana=(x-stangaAutoFill)/laturaAutoFill+1;
            if (tablaAutoFill[linia][coloana]==0)
            {

                setcolor(FUNDAL);
                setfillstyle(SOLID_FILL,FUNDAL);
                x1=stangaAutoFill+laturaAutoFill*(coloana-1);
                y1=susAutoFill+laturaAutoFill*(linia-1);
                x2=x1+laturaAutoFill;
                y2=y1+laturaAutoFill;
                xmijloc=(x1+x2)/2;
                ymijloc=(y1+y2)/2;
                int ok=1;
                for(int i=0;i<nrcercuriAutoFill;i++)
                {
                    if(cercuriAutoFill[i].x==xmijloc && cercuriAutoFill[i].y==ymijloc)
                    {
                        ok=0;
                    }
                }
                // afisez patrat
                if(ok==1){

                    nrcercuriAutoFill++;
                    cercuriAutoFill[nrcercuriAutoFill].x=xmijloc;
                    cercuriAutoFill[nrcercuriAutoFill].y=ymijloc;
                    setcolor(Vculori[nrcercuriAutoFill]);
                    setfillstyle(SOLID_FILL,Vculori[nrcercuriAutoFill]);

                    circle(cercuriAutoFill[nrcercuriAutoFill].x,cercuriAutoFill[nrcercuriAutoFill].y,std::min(widthAutoFill,heightAutoFill)*1/25);

                    floodfill(cercuriAutoFill[nrcercuriAutoFill].x,cercuriAutoFill[nrcercuriAutoFill].y,Vculori[nrcercuriAutoFill]);

                    tablaAutoFill[linia][coloana]=nrcercuriAutoFill;
                    a[linia][coloana]=nrcercuriAutoFill;
                    q[nrcercuriAutoFill].l=linia;
                    q[nrcercuriAutoFill].c=coloana;

                    drawBottomText();
                }
            }
        }
    }

}

void deleteLastCircle()
{

    if(nrcercuriAutoFill>0){
        setfillstyle(SOLID_FILL,FUNDAL);
        bar(cercuriAutoFill[nrcercuriAutoFill].x-laturaAutoFill/2+1 , cercuriAutoFill[nrcercuriAutoFill].y-laturaAutoFill/2+1,
         cercuriAutoFill[nrcercuriAutoFill].x+laturaAutoFill/2-1 , cercuriAutoFill[nrcercuriAutoFill].y+laturaAutoFill/2-1);

        cercuriAutoFill[nrcercuriAutoFill].x=0;
        cercuriAutoFill[nrcercuriAutoFill].y=0;

        tablaAutoFill[q[nrcercuriAutoFill].l][q[nrcercuriAutoFill].c]=0;
        a[q[nrcercuriAutoFill].l][q[nrcercuriAutoFill].c]=0;
        q[nrcercuriAutoFill].l=0;
        q[nrcercuriAutoFill].c=0;

        nrcercuriAutoFill--;
    }
}

void num10secs()
{
    delay(5000);
}

void backAndDraw()
{
    back(q[1].l,q[1].c,1,2);
    drawSquares();
}
int autoFill ()
{
    settextjustify(CENTER_TEXT, CENTER_TEXT);
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 4);
    setcolor(BLACK);

    outtextxy(getwindowwidth()/2, getwindowheight()/10, displayText[displayLanguage][31]);


    setbkcolor(FUNDAL);
    drawBottomText();
    nrcercuriAutoFill=0;
    foundSol=0;
    for(int i=1;i<=8;i++)
        {   q[i].c=q[i].l=0;
            for(int j=1;j<=8;j++)
            tablaAutoFill[i][j]=a[i][j]=0;
        }
    int ok = 1;
    while( ok==1)
    {//int opt = menuHandler();
        if(nrcercuriAutoFill <= 7) punereCerc();


        if(kbhit())
        {
            char c;
            c = getch();
            cout<<c;
            if(c==8) {deleteLastCircle(); drawBottomText();}
            if(c==27 && nrcercuriAutoFill<8) ok=0;
            if(c==27 && nrcercuriAutoFill==8) ok=2;
            if(c==13 && nrcercuriAutoFill==8) ok=2;

        }
    }
    drawBottomText();
    if(ok==2){
        thread t1(num10secs);
        thread t2(backAndDraw);
        t2.detach();
        t1.join();
        return foundSol;
    }

}

void desenAutoFill()
{
    numarAutoFill = 8;
    widthAutoFill=std::min(getwindowheight(),getwindowwidth())*3/4;
    laturaAutoFill=widthAutoFill/numarAutoFill;
    heightAutoFill=widthAutoFill;
    susAutoFill=(getwindowheight()-widthAutoFill)/2;
    stangaAutoFill=(getwindowwidth()- widthAutoFill)/2;
    setbkcolor(FUNDAL);
    clearviewport();
    setcolor(BLUE);

    for (int i=1; i<=numarAutoFill; i++)
        for (int j=1; j<=numarAutoFill; j++)
            rectangle(stangaAutoFill+laturaAutoFill*(i-1),susAutoFill+laturaAutoFill*(j-1),stangaAutoFill+laturaAutoFill*i,susAutoFill+laturaAutoFill*j);

}

