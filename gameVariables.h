int currentResolutionInd=0;
int resolutionsWidths[5] = {960, 1280, 1366,  1440, 0};
int resolutionsHeights[6] = {640, 720, 768,  800, 0};
int volumeVec[6] = {0, 65535/20,65535/10,  65535/5, 65535/2, 65535};
int settingsOption[6]={5, 1, 1, 0, 6};
int menuBackgroundColor = WHITE;
char menuBackgroundImage[100] ="./images/backMenu.jpg";
int Vculori [9]= {RED,GREEN,YELLOW,LIGHTCYAN,BLUE,BLACK,WHITE,BROWN,LIGHTRED};

char playersName[5][100] = {"", "1", "2", "", ""};
int playersScore[5]={0, 5, 10, 0, 0};
int menuDrawn=0;
int Playercolor[3]={-1, WHITE, BLUE};
int gameOn;

char displayText[3][100][255];
int displayLanguage=0;
#include <fstream>

void initDispText()
{
    std::ifstream LGromana("./lang/romana.lb"); // 0
    std::ifstream LGfranceza("./lang/franceza.lb");  // 1
    std::ifstream LGengleza("./lang/engleza.lb"); // 2
    for(int i=0;i<=36;i++)
    {
        LGromana.get(displayText[0][i], 200);
        LGromana.get();
        LGfranceza.get(displayText[1][i], 200);
        LGfranceza.get();
        LGengleza.get(displayText[2][i], 200);
        LGengleza.get();
    }


}
void intToChar(char c[100], int x)
{
    strcpy(c, "");
    char p[100];
    while(x)
    {
        strcpy(p, c);
        strcpy(c+1, p);
        c[0]=x%10+'0';
        x/=10;
    }
}
