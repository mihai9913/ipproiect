
#include <fstream>




char highScoresNames[11][100];
int highScores[11];
int highScoresNum;
void readHighScores()
{
    std::ifstream highScoreFileIn("highscores.hs");
    highScoresNum=0;

    while(highScoreFileIn>>highScores[highScoresNum])
    {
        highScoreFileIn.get();
        highScoreFileIn.get(highScoresNames[highScoresNum], 100);

        highScoresNum++;
    }


    highScoreFileIn.close();



}

void addHS()
{
    readHighScores();
    std::ofstream highScoreFileOut("highscores.hs");
    int HS;
    char name[100];
    HS = std::max(playersScore[1], playersScore[2]);
    if(HS==playersScore[1])
        strcpy(name, playersName[1]);
    else
        strcpy(name, playersName[2]);

    highScores[highScoresNum] = HS;
    strcpy(highScoresNames[highScoresNum], name);
    highScoresNum++;



    for(int i=0;i<highScoresNum-1;i++)
        for(int j=i+1;j<highScoresNum;j++)
            if(highScores[i]<highScores[j])
            {
                std::swap(highScores[i], highScores[j]);
                std::swap(highScoresNames[i], highScoresNames[j]);
            }
    for(int i=0;i<highScoresNum && i<10;i++)
    {
        highScoreFileOut<<highScores[i]<<std::endl;
        highScoreFileOut<<highScoresNames[i]<<std::endl;
    }
    highScoreFileOut.close();


}


void getPlayerName(int id)
{

    delay(200);
    setfillstyle(SOLID_FILL, YELLOW);
    setbkcolor(YELLOW);
    setcolor(BLACK);
    bar(getwindowwidth()/2-200, getwindowheight()/2-30, getwindowwidth()/2+200, getwindowheight()/2+60);
    settextjustify(CENTER_TEXT, CENTER_TEXT);
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 2);
    outtextxy(getwindowwidth()/2, getwindowheight()/2+50, displayText[displayLanguage][19]);
    // GET Player 1 name
    char x[10], p[100];
    strcpy(p, displayText[displayLanguage][20]);
    strcat(p, " ");
    intToChar(x, id);
    strcat(p, x);
    strcat(p, displayText[displayLanguage][21]);
    outtextxy(getwindowwidth()/2, getwindowheight()/2-10, p);
    setfillstyle(SOLID_FILL, LIGHTBLUE);
    bar(getwindowwidth()/2-150, getwindowheight()/2, getwindowwidth()/2+150, getwindowheight()/2+25);
    setbkcolor(LIGHTBLUE);
    setfillstyle(SOLID_FILL, YELLOW);

    char c=getch();
    if(c!=13)
        for(int i=0;i<strlen(playersName[id]);i++)
            playersName[id][i]='\0';

    while(c!=13)
    {


        setfillstyle(SOLID_FILL, LIGHTBLUE);
        bar(getwindowwidth()/2-150, getwindowheight()/2, getwindowwidth()/2+150, getwindowheight()/2+25);

        if(strlen(playersName[id])<15)
            {
                strcat(playersName[id]," ");
                playersName[id][strlen(playersName[id])-1]=c;
            }
        if(c==8)
            {
            strcpy(playersName[id]+strlen(playersName[id])-2, "");
            }
        outtextxy(getwindowwidth()/2, getwindowheight()/2+18, playersName[id]);
        c=getch();

    }
    if(settingsOption[2]==1)
        for(float i=45;i>=0;i-=0.02f)
        {
                setcolor(menuBackgroundColor);
                line(getwindowwidth()/2-200, getwindowheight()/2+15-i, getwindowwidth()/2+200, getwindowheight()/2+15-i);
                line(getwindowwidth()/2-200, getwindowheight()/2+15+i, getwindowwidth()/2+200, getwindowheight()/2+15+i);
        }
    else
    {
        setfillstyle(SOLID_FILL, menuBackgroundColor);
        bar(getwindowwidth()/2-200, getwindowheight()/2-30, getwindowwidth()/2+200, getwindowheight()/2+60);
    }
}
void endGame()
{
    settextjustify(CENTER_TEXT, CENTER_TEXT);
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 5);
    setcolor(BLACK);
    setfillstyle(SOLID_FILL, YELLOW);

    fillellipse(getwindowwidth()/2, getwindowheight()/2, 300, 150);
    setbkcolor(YELLOW);
    outtextxy(getwindowwidth()/2, getwindowheight()/2-25, displayText[displayLanguage][22]);
    char p[100];
    strcpy(p, displayText[displayLanguage][20]);
    if(playersScore[1]!=0 || playersScore[2]!=0)
    {

        if(playersScore[1]>playersScore[2])
            strcat(p, playersName[1]);
        else
            strcat(p, playersName[2]);

        strcat(p, displayText[displayLanguage][23]);
    }
    else strcpy(p, displayText[displayLanguage][24]);
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 4);
    outtextxy(getwindowwidth()/2, getwindowheight()/2+30, p);

    char c=getch();
    while(c!=13)
        c=getch();


    gameOn=0;
    menuDrawn=0;

}
void getPlayersName()
{

    getPlayerName(1);
    getPlayerName(2);

}


