
#define settings5BARS 1
#define settingsONOFF 2
#define settingsREZ 3
#define settingsIMG 4

#define menuMain 0
#define menuSettings 1
#define menuStart 2
#define menuCredits 3
#define menuHs 4

struct barProps{
    int xFrom, yFrom;
    int xTo, yTo;
    int color, overColor;
    char text[100];

    bool over = false;
};




int settingsType[5] = {
    settings5BARS, settingsONOFF, settingsONOFF, settingsIMG, settingsREZ
};

int settingsOptionsMax[6] = {5, -1, -1, 2, 4};
barProps menuBars[10][100];
int cntMenuBars[10];
bool anim = false;


void initMenu()
{
    // 0 - main menu
    // 1 - settings menu
    cntMenuBars[menuMain] = 5;
    cntMenuBars[menuSettings] = 6;
    cntMenuBars[menuStart] = 4;
    cntMenuBars[menuHs] = 12;


    for(int i=0;i<cntMenuBars[menuHs];i++)
    {

        menuBars[menuHs][i].yFrom=getwindowheight()/8 + i*(getwindowheight()-100)/cntMenuBars[menuHs]+2;
        menuBars[menuHs][i].xFrom=getwindowwidth()/4;
        menuBars[menuHs][i].xTo= menuBars[menuHs][i].xFrom+ getwindowwidth()/2;

        menuBars[menuHs][i].yTo=menuBars[menuHs][i].yFrom+(getwindowheight()-100)/cntMenuBars[menuHs]-2;

        menuBars[menuHs][i].color = GREEN;
        menuBars[menuHs][i].overColor =GREEN;

    }
    strcpy(menuBars[menuHs][0].text, displayText[displayLanguage][15]);
    menuBars[menuHs][0].color = menuBars[menuHs][0].overColor=YELLOW;
    strcpy(menuBars[menuHs][11].text, displayText[displayLanguage][16]);
    menuBars[menuHs][11].color = YELLOW;
    menuBars[menuHs][11].overColor = RED;


    for(int i=0;i<cntMenuBars[menuMain];i++)
    {

        menuBars[menuMain][i].yFrom=getwindowheight()/5 + i*(getwindowheight()-200)/cntMenuBars[0]+10;
        menuBars[menuMain][i].xFrom=getwindowwidth()/4;
        menuBars[menuMain][i].xTo= menuBars[menuMain][i].xFrom+ getwindowwidth()/2;

        menuBars[menuMain][i].yTo=menuBars[menuMain][i].yFrom+(getwindowheight()-200)/cntMenuBars[0]-10;

        strcpy( menuBars[menuMain][i].text, displayText[displayLanguage][i] );
        menuBars[menuMain][i].color = YELLOW;
        menuBars[menuMain][i].overColor = RED;

    }

    for(int i=0;i<cntMenuBars[menuSettings];i++)
    {

        menuBars[menuSettings][i].yFrom=getwindowheight()/5 + i*(getwindowheight()-200)/cntMenuBars[1]+10;
        menuBars[menuSettings][i].xFrom=getwindowwidth()/4;
        if(i<cntMenuBars[menuSettings]-2) menuBars[menuSettings][i].xTo= menuBars[menuSettings][i].xFrom+ getwindowwidth()*3/8;
        else menuBars[menuSettings][i].xTo= menuBars[menuSettings][i].xFrom+ getwindowwidth()/2;
        menuBars[menuSettings][i].yTo=menuBars[menuSettings][i].yFrom+(getwindowheight()-200)/cntMenuBars[menuSettings]-10;

        strcpy( menuBars[menuSettings][i].text,displayText[displayLanguage][5+i] );
        menuBars[menuSettings][i].color = YELLOW;
        menuBars[menuSettings][i].overColor = RED;

    }

        menuBars[menuStart][0].xFrom = getwindowwidth()/6+50;
        menuBars[menuStart][0].xTo = getwindowwidth()/2-10;
        menuBars[menuStart][0].yFrom = getwindowheight()/3;
        menuBars[menuStart][0].yTo = getwindowheight()/2;

        menuBars[menuStart][1].xFrom = getwindowwidth()/2+10;
        menuBars[menuStart][1].xTo = getwindowwidth()*5/6-50;
        menuBars[menuStart][1].yFrom = getwindowheight()/3;
        menuBars[menuStart][1].yTo = getwindowheight()/2;

        menuBars[menuStart][2].xFrom = getwindowwidth()/4;
        menuBars[menuStart][2].xTo = getwindowwidth()*3/4;
        menuBars[menuStart][2].yFrom = getwindowheight()/2+10;
        menuBars[menuStart][2].yTo = getwindowheight()*2/3;

        menuBars[menuStart][3].xFrom = getwindowwidth()/4;
        menuBars[menuStart][3].xTo = getwindowwidth()*3/4;
        menuBars[menuStart][3].yFrom = getwindowheight()*2/3+10;
        menuBars[menuStart][3].yTo = getwindowheight()*5/6;


        for(int i=0;i<cntMenuBars[menuStart];i++){
            strcpy( menuBars[menuStart][i].text,displayText[displayLanguage][11+i] );
            menuBars[menuStart][i].color = YELLOW;
            menuBars[menuStart][i].overColor = RED;

        }
    cntMenuBars[menuCredits]=1;
    menuBars[menuCredits][0].xFrom=getwindowwidth()/4;
    menuBars[menuCredits][0].xTo=getwindowwidth()*3/4;
    menuBars[menuCredits][0].yFrom=getwindowheight()-100;
    menuBars[menuCredits][0].yTo=getwindowheight();
    strcpy(menuBars[menuCredits][0].text, displayText[displayLanguage][16]);
    menuBars[menuCredits][0].color = YELLOW;
    menuBars[menuCredits][0].overColor = RED;

}
void drawSettionsOptions(int i)
{

    int xFrom = getwindowwidth()*5/8+10;
    int xTo = getwindowwidth() *3/4-10;
    int yFrom = menuBars[menuDrawn][i].yFrom+10;
    int yTo = menuBars[menuDrawn][i].yTo-10;

    if(settingsType[i] == settingsIMG)
    {
       int rectWidth = std::min(xTo-xFrom, yTo-yFrom)/2;
        int xx = (xFrom+xTo)/2;
        int yy = (yFrom+yTo)/2;
        setfillstyle(SOLID_FILL, BLUE);
        bar(xx-rectWidth-20, yy-rectWidth-5, xx+rectWidth+20, yy+rectWidth+5);
        if(displayLanguage==0)
            readimagefile("./images/romania.jpg",xx-rectWidth-20, yy-rectWidth-5, xx+rectWidth+20, yy+rectWidth+5 );
        else if(displayLanguage==1)
            readimagefile("./images/franceza.jpg",xx-rectWidth-20, yy-rectWidth-5, xx+rectWidth+20, yy+rectWidth+5 );
        else readimagefile("./images/engleza.jpg",xx-rectWidth-20, yy-rectWidth-5, xx+rectWidth+20, yy+rectWidth+5 );
    }

    else if(settingsType[i] == settings5BARS )
    {
        int barlenght = (xTo-xFrom)/settingsOptionsMax[i];
        int k;
        for(k=0;k<settingsOption[i];k++)
        {
            setfillstyle(SOLID_FILL, YELLOW);
            bar(xFrom + barlenght*k, yFrom+(settingsOptionsMax[i]-1-k)*(yTo-yFrom)/settingsOptionsMax[i], xFrom+k*barlenght+barlenght-5, yTo);
        }
        for(;k<=settingsOptionsMax[i];k++)
             {
            setfillstyle(SOLID_FILL, menuBackgroundColor);
            bar(xFrom + barlenght*k, yFrom, xFrom+k*barlenght+barlenght-5, yTo);
        }
    }
   else  if(settingsType[i] == settingsONOFF)
    {
        int rectWidth = std::min(xTo-xFrom, yTo-yFrom)/2;
        int xx = (xFrom+xTo)/2;
        int yy = (yFrom+yTo)/2;
        if(settingsOption[i] == 1)
        {
            setfillstyle(SOLID_FILL, YELLOW);
            bar(xx-rectWidth, yy-rectWidth, xx+rectWidth, yy+rectWidth );
            setfillstyle(SOLID_FILL, BLACK);
            bar(xx-rectWidth+10, yy-rectWidth+10, xx+rectWidth-10, yy+rectWidth-10);
        }
        else
        {
            setfillstyle(SOLID_FILL, YELLOW);
            bar(xx-rectWidth, yy-rectWidth, xx+rectWidth, yy+rectWidth);

        }
    }




}
void drawBar(barProps b)
{
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 1);
    setcolor(BLACK);
    if(b.over==1)
        {
                setfillstyle( SOLID_FILL, b.overColor);
                setbkcolor(b.overColor);
                 readimagefile("./images/over.jpg",b.xFrom, b.yFrom,b.xTo ,b.yTo);

        }
        else
            {
                 setfillstyle( SOLID_FILL,b.color);
                setbkcolor(b.color);
                 readimagefile("./images/default.jpg",b.xFrom, b.yFrom,b.xTo ,b.yTo);
            }

   // bar(b.xFrom, b.yFrom,b.xTo ,b.yTo);
    settextjustify(1, 1);
    if(strcmp(b.text, displayText[displayLanguage][9])==0)
    {
        char rezTxt[100]="", aux[10]="";
        intToChar(aux,resolutionsWidths[currentResolutionInd] );

        strcat(rezTxt, aux);
        strcat(rezTxt, " X ");
        intToChar(aux,resolutionsHeights[currentResolutionInd]);

        strcat(rezTxt, aux);
        if(settingsOption[4]==4) strcpy(rezTxt, displayText[displayLanguage][36]);
        outtextxy(b.xFrom+(b.xTo-b.xFrom)/2, (b.yFrom+b.yTo)/2+20, rezTxt);

    }
    outtextxy(b.xFrom+(b.xTo-b.xFrom)/2, (b.yFrom+b.yTo)/2, b.text);
    setfillstyle( SOLID_FILL, menuBackgroundColor);
}

bool mouseOverButton(barProps bar)
{
    int x=mousex();
    int y=mousey();
    return x>bar.xFrom && y > bar.yFrom && x< bar.xTo && y<bar.yTo;
}
void drawCredits()
{
    setbkcolor(menuBackgroundColor);
    settextjustify(CENTER_TEXT, CENTER_TEXT);
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 3);
    setcolor(BLACK);
    outtextxy(getwindowwidth()/2, getwindowheight()/4, displayText[displayLanguage][17]);
    readimagefile("./images/blaga.jpg",getwindowwidth()/2-300,getwindowheight()/4+50, getwindowwidth()/2-10,getwindowheight()/4+350);
    readimagefile("./images/catalin.jpg",getwindowwidth()/2+10,getwindowheight()/4+50, getwindowwidth()/2+300,getwindowheight()/4+350);
}
void openMenu()
{
        if(menuDrawn == menuCredits)
            drawCredits();

        for(int i=0;i<cntMenuBars[menuDrawn];i++)
        {
            drawBar(menuBars[menuDrawn][i]);
            if(menuDrawn==1)
                drawSettionsOptions(i);
           if(settingsOption[2]) delay(200);

        }
    delay(100);
    anim=false;

}
void closeMenu(int nextMenu)
{
    if(menuDrawn==menuHs)
    {
        setfillstyle(SOLID_FILL, menuBackgroundColor);
        bar(0, getwindowheight()/8, getwindowwidth(), getwindowheight());
    }
    else
    if(settingsOption[2]==1)
        {
            for(float h = 0; h<=(menuBars[menuDrawn][0].yTo-menuBars[menuDrawn][0].yFrom)/2+10;h+=0.2f)
            for(int i=0;i<cntMenuBars[menuDrawn];i++)
            {
                setfillstyle(SOLID_FILL, menuBackgroundColor);
                setcolor(menuBackgroundColor);
                int to=menuBars[menuDrawn][i].xTo;
                if(menuDrawn==menuSettings)
                    to=menuBars[menuDrawn][cntMenuBars[menuSettings]-1].xTo;
                line(menuBars[menuDrawn][i].xFrom, menuBars[menuDrawn][i].yFrom+h,to, menuBars[menuDrawn][i].yFrom+h);
                line(menuBars[menuDrawn][i].xFrom, menuBars[menuDrawn][i].yTo-h, to, menuBars[menuDrawn][i].yTo-h);
            }

        }
    else
    {
        setfillstyle(SOLID_FILL, menuBackgroundColor);
        bar(0, getwindowheight()/5, getwindowwidth(), getwindowheight());


    }
   // readimagefile(menuBackgroundImage,0, 0, getwindowwidth(), getwindowheight() );
    if(nextMenu!=-1)
   {
        menuDrawn = nextMenu;
        openMenu();
    }

}

int drawMenu()
{


    setfillstyle(SOLID_FILL, menuBackgroundColor);
    bar(0, 0, getwindowwidth(), getwindowheight());
    readimagefile(menuBackgroundImage,0, 0, getwindowwidth(), getwindowheight() );
    if(menuDrawn == menuSettings)
         for(int i=0;i<cntMenuBars[menuSettings];i++)
                        drawSettionsOptions(i);

    setbkcolor(menuBackgroundColor);
    settextjustify(CENTER_TEXT, CENTER_TEXT);
    settextstyle(SIMPLEX_FONT , HORIZ_DIR, 4);
    setcolor(BLACK);
    outtextxy(getwindowwidth()/2, getwindowheight()/10, displayText[displayLanguage][18]);

    for(int i=0;i<cntMenuBars[menuDrawn];i++)
    {

        setcolor(BLACK);
        if(!anim) drawBar(menuBars[menuDrawn][i]);

    }

    while(1)
    {


            // MENU CLICK
        if(ismouseclick(WM_LBUTTONDOWN) && anim==false)
        {


            clearmouseclick(WM_LBUTTONDOWN);
            //highscores
            if(menuDrawn==menuHs && anim==false)
            {
                if(mouseOverButton(menuBars[menuHs][11]))
                {
                    beep();

                    menuBars[menuHs][11].over = false;
                    drawBar(menuBars[menuHs][11]);
                    anim=true;
                    bar(0, getwindowheight()/5, getwindowwidth(), getwindowheight()-100);
                    closeMenu(menuMain);
                }
            }
            else
            //credits
            if(menuDrawn==menuCredits && anim==false)
            {
                if(mouseOverButton(menuBars[menuCredits][0]))
                {
                    beep();

                    menuBars[menuCredits][0].over = false;
                    drawBar(menuBars[menuCredits][0]);
                    anim=true;
                    bar(0, getwindowheight()/5, getwindowwidth(), getwindowheight()-100);
                    closeMenu(menuMain);
                }
            }
            else
            // menu start
            if(menuDrawn==menuStart && anim==false)
            {
                //goto game vs player
                if(mouseOverButton(menuBars[menuStart][0]) )
                {
                    beep();
                    setfillstyle(SOLID_FILL, BLACK);
                    //bar(0, 0, getwindowwidth(), getwindowheight());1
                    closeMenu(-1);
                    return 1;


                }
                //goto game vs AI
                if(mouseOverButton(menuBars[menuStart][1]) )
                {
                    beep();
                    setfillstyle(SOLID_FILL, BLACK);
                    //bar(0, 0, getwindowwidth(), getwindowheight());1
                    closeMenu(-1);
                    return 3;


                }
                 //goto game vs comouter - autofil
                if(mouseOverButton(menuBars[menuStart][2]) )
                {
                    beep();
                    setfillstyle(SOLID_FILL, BLACK);
                    //bar(0, 0, getwindowwidth(), getwindowheight());1
                    closeMenu(-1);
                    return 2;


                }
                //goto back
                if(mouseOverButton(menuBars[menuStart][3]) )
                {
                    beep();
                    menuBars[menuStart][3].over = false;
                    drawBar(menuBars[menuStart][3]);
                    anim=true;
                    closeMenu(menuMain);

                }




            }
            // menu main
            else if(menuDrawn==menuMain && anim==false)
            {
                //goto Start
                if( mouseOverButton(menuBars[menuMain][0]) )
                {
                    beep();
                    menuBars[menuMain][0].over = false;
                    drawBar(menuBars[menuMain][0]);
                    anim=true;
                    closeMenu(menuStart);
                }
                // goto Options
                else if(mouseOverButton(menuBars[menuMain][1]) )
                {
                    beep();
                    menuBars[menuMain][1].over = false;
                    drawBar(menuBars[menuMain][1]);
                    anim=true;
                    closeMenu(menuSettings);
                }
                //go to HighScores
                 else if(mouseOverButton(menuBars[menuMain][2]) )
                {
                    beep();
                    menuBars[menuMain][2].over = false;
                    drawBar(menuBars[menuMain][2]);
                    anim=true;
                    readHighScores();
                    for(int i=1;i<=10;i++)
                    {
                        char txt[100] = "";
                        strcpy(txt, highScoresNames[i-1]);
                        strcat(txt, ": ");
                        char x[10];
                        intToChar(x, highScores[i-1]);
                        strcat(txt, x);
                        strcpy( menuBars[menuHs][i].text,txt );
                    }
                    closeMenu(menuHs);
                }
                 // goto Credits
                else if(mouseOverButton(menuBars[menuMain][3]) )
                {
                    beep();
                    menuBars[menuMain][3].over = false;
                    drawBar(menuBars[menuMain][3]);
                    anim=true;
                    closeMenu(menuCredits);
                }
                // exit
                else if(mouseOverButton(menuBars[menuMain][4]) )
                {

                    beep();
                     delay(700);
                    return -1;
                }
            }
            // menu settings
            else if(menuDrawn==menuSettings && anim==false)
            {

                //volume
                if(mouseOverButton(menuBars[menuSettings][0]))
                {
                    beep();
                    settingsOption[0]++;
                    if(settingsOption[0]==settingsOptionsMax[0]+1) settingsOption[0]=0;
                    std::cout<<settingsOption[0]<<std::endl;
                    setNewVolume();
                    drawSettionsOptions(0);
                }
                //sfx
                else if(mouseOverButton(menuBars[menuSettings][1]))
                {
                    beep();
                    settingsOption[1]=1-settingsOption[1];
                    drawSettionsOptions(1);
                }
                //animations
                else if(mouseOverButton(menuBars[menuSettings][2]))
                {
                    beep();
                    settingsOption[2]=1-settingsOption[2];
                    drawSettionsOptions(2);
                }
                //Language
                else if(mouseOverButton(menuBars[menuSettings][3]))
                {
                    beep();
                    settingsOption[3]++;
                    displayLanguage++;
                    if(displayLanguage==3) displayLanguage=0;
                    if(settingsOption[3]==settingsOptionsMax[3]+1) settingsOption[3]=1;
                    drawSettionsOptions(3);
                }
                //resize
                else if(mouseOverButton(menuBars[menuSettings][4]) )
                {
                    beep();
                    closegraph();
                    std::cout<<2;
                    currentResolutionInd++;

                    if(currentResolutionInd>4) currentResolutionInd=0;
                    settingsOption[4]=currentResolutionInd;
                    initwindow(resolutionsWidths[currentResolutionInd], resolutionsHeights[currentResolutionInd]);
                    initMenu();
                    initDispText();
                    return drawMenu();

                }
                //back
                else if(mouseOverButton(menuBars[menuSettings][5]) )
                {
                    beep();
                    menuBars[menuDrawn][5].over = false;
                    drawBar(menuBars[menuDrawn][5]);
                    anim=true;
                    closeMenu(menuMain);
                }
            }

        }

        for(int i=0;i<cntMenuBars[menuDrawn];i++)
        {

                if(mouseOverButton(menuBars[menuDrawn][i]))
                {
                // MENU OVER
                    if(menuBars[menuDrawn][i].over == false)
                    {

                        menuBars[menuDrawn][i].over = true;
                        if(!anim) drawBar(menuBars[menuDrawn][i]);
                    }
                }
                else
                {
                    if(menuBars[menuDrawn][i].over == true)
                    {

                        menuBars[menuDrawn][i].over = false;
                        if(!anim) drawBar(menuBars[menuDrawn][i]);
                    }

                }
            }


    }

}
