#include <time.h>

#define MAX 10
#define FUNDAL CYAN
using namespace std;

int stanga,sus,width,height,latura, numar;
bool gata;

int TablaDeJoc[MAX][MAX];
int activePlayer;

int VculoriOC [20];
int colorflag=-1;

int earlyEnd();
int playersLines[10];

int nrc;


int d1[]={0,0,-1,1};
int d2[]={1,-1,0,0};
int v[10][10], v2[10][10],  x[10][10];
int filRe;
int filFoundColor;

struct cerc
{
    float x;
    float y;


} cercuri [8];




struct CentreCasuta
{
    float x;
    float y;

} CentreCasute[64];

// MOTHER OF THE FILL ALGORITHM
int fillColor(int i, int j, int col)
{
    filRe--;
    if(filRe>=0)
    {
        v2[i][j]=activePlayer;
        TablaDeJoc[i][j]=200+activePlayer;

        setfillstyle(SOLID_FILL, BLACK);
        bar(stanga+(j-1)*latura+12, sus+(i-1)*latura+12,stanga+j*latura-12, sus+i*latura-12 );

        setfillstyle(SOLID_FILL, col);
        bar(stanga+(j-1)*latura+15, sus+(i-1)*latura+15,stanga+j*latura-15, sus+i*latura-15 );

        for(int k=0;k<=3;k++)
        {
            int ll = i+d1[k];
            int cc = j+d2[k];
            if(TablaDeJoc[ll][cc]==activePlayer && v2[ll][cc]==0)
                fillColor(ll, cc, col);
        }
    }
}

int fil(int i, int j)
{   v[i][j]=activePlayer;
    int nr=0;
    for(int k=0;k<=3;k++)
    {
        int ll = i+d1[k];
        int cc = j+d2[k];

        if(TablaDeJoc[ll][cc]==activePlayer && v[ll][cc]==0)
        {
             nr+=fil(ll, cc);
             nr++;
        }
    }
    return nr;

}

int filFindColor(int i, int j)
{   v[i][j]=activePlayer;

    for(int k=0;k<=3;k++)
    {
        int ll = i+d1[k];
        int cc = j+d2[k];
        if(TablaDeJoc[ll][cc]>=100 && TablaDeJoc[ll][cc]<200 && VculoriOC[TablaDeJoc[ll][cc]-100]==0 && colorflag==-1)
        {
            VculoriOC[TablaDeJoc[ll][cc]-100]=1;
            colorflag = TablaDeJoc[ll][cc]-100;
        }

        else if((TablaDeJoc[ll][cc]==activePlayer ) && v[ll][cc]==0)
           filFindColor(ll, cc);

    }

}

void filEnd(int i, int j, int p)
{
    x[i][j]=-1;

    for(int k=0;k<=3;k++)
    {
        int ll = i+d1[k];
        int cc = j+d2[k];
        if(x[ll][cc]>=100 && x[ll][cc]<200)
            filFoundColor=1;
        if((ll>=1 && ll<=8 && cc>=1 && cc<=8) && (x[ll][cc]==p || x[ll][cc]==0))
           filEnd(ll, cc, p);

    }
}

int filVerif(int i, int j)
{
    int nr=1;
    x[i][j]=-2;
    for(int k=0;k<=3;k++)
    {
        int ll = i+d1[k];
        int cc = j+d2[k];

        if((ll>=1 && ll<=8 && cc>=1 && cc<=8) && x[ll][cc]==-1)
           nr+=filVerif(ll, cc);

    }
    return nr;
}


void drawPlayers()
{
    setfillstyle(SOLID_FILL, FUNDAL);
   // bar(0, 70, stanga, getwindowheight()-200);

    char s1[100], s2[100];
    intToChar(s1, playersScore[1]);
    intToChar(s2, playersScore[2]);
    setfillstyle(SOLID_FILL, Playercolor[1]);
    bar(40, 210, 60, 230);
        setfillstyle(SOLID_FILL, Playercolor[2]);
    bar(40, 310, 60,330);
    if(activePlayer==1)
    {
        setcolor(BLACK);
        settextstyle(GOTHIC_FONT, HORIZ_DIR, 2);
        outtextxy(100, 200, playersName[1]);

        settextstyle(GOTHIC_FONT, HORIZ_DIR, 1);
        outtextxy(100, 300, playersName[2]);

    }
    if(activePlayer==2)
    {
        setcolor(BLACK);
        settextstyle(GOTHIC_FONT, HORIZ_DIR, 1);
        outtextxy(100, 200, playersName[1]);

        settextstyle(GOTHIC_FONT, HORIZ_DIR, 2);
        outtextxy(100, 300, playersName[2]);

    }
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 2);
    outtextxy(100, 230, s1);
    outtextxy(100, 330, s2);
}
void addNewScore()
{
    if(playersLines[activePlayer]==1)
        playersScore[activePlayer]+=40;
    else if(playersLines[activePlayer]==2)
        playersScore[activePlayer]+=30;
    else if(playersLines[activePlayer]==3)
        playersScore[activePlayer]+=20;
    else if(playersLines[activePlayer]==4)
        playersScore[activePlayer]+=10;
    else playersScore[activePlayer]+=5;
}
void cl(int v[10][10])
{
    for(int i=0;i<10;i++)
        for(int j=0;j<10;j++)
        v[i][j]=0;
}


void PunereCercuriRandom ()
{
    int n=64;
    setcolor(RED);
    for(int i=1; i<=8; i++)
    {
        srand (time(NULL));
        int Random=  (rand ()%n)+1;

        cercuri[i].x=CentreCasute[Random].x;
        cercuri[i].y=CentreCasute[Random].y;

      for(int j=Random;j<n;j++)
      {
         CentreCasute[j].x=CentreCasute[j+1].x;
         CentreCasute[j].y=CentreCasute[j+1].y;
      }
        n--;
    }

    int Culori[8]= {RED,BLUE,BLACK,GREEN,BROWN,MAGENTA,WHITE,LIGHTBLUE};
    int k=1;
       cl(TablaDeJoc);

    for(int i=1; i<=8; i++)
    {
        //poz cercuri in matrice
        int ii=(cercuri[i].y*2-2*sus+latura)/2/latura;
        int jj=(cercuri[i].x*2-2*stanga+latura)/2/latura;

        TablaDeJoc[ii][jj]=100+Vculori[k];
        setfillstyle(SOLID_FILL,Vculori[k]);
        delay(100);
        circle(cercuri[i].x,cercuri[i].y,std::min(width,height)*1/25);
        floodfill(cercuri[i].x,cercuri[i].y,RED);
        k++;

    }


}

int punerePiesa()
{
    int linia,coloana,x,y;
    int x1, y1, x2, y2;
    int xmijloc, ymijloc;
    if(ismouseclick(WM_LBUTTONDOWN))
    {
        clearmouseclick(WM_LBUTTONDOWN);
        x=mousex();
        y=mousey();
        if (!(x>=stanga && x<=stanga+width && y>=sus&&y<=sus+height))
            gata=true;
        else
        {
            linia=(y-sus)/latura+1;
            coloana=(x-stanga)/latura+1;
            if (TablaDeJoc[linia][coloana]==0)
            {

                setcolor(FUNDAL);
                setfillstyle(SOLID_FILL,FUNDAL);
                x1=stanga+latura*(coloana-1);
                y1=sus+latura*(linia-1);
                x2=x1+latura;
                y2=y1+latura;
                xmijloc=(x1+x2)/2;
                ymijloc=(y1+y2)/2;
                int ok=1;
                for(int i=1;i<=8;i++)
                {

                    if(cercuri[i].x==xmijloc&&cercuri[i].y==ymijloc)
                    {
                        ok=0;
                    }
                }
                // afisez patrat
                if(ok==1){
                setcolor(Playercolor[activePlayer]);
                setfillstyle(SOLID_FILL,Playercolor[activePlayer]);
                bar(x1+10,y1+10,x2-10,y2-10);
                TablaDeJoc[linia][coloana]=activePlayer;

                cl(v);
                int aa = fil(linia, coloana);

                if(aa>=6)
                    {
                        cl(v);
                        colorflag=-1;
                        filFindColor(linia, coloana);
                        if(colorflag!=-1)
                        {
                            cl(v2);
                            filRe=7;
                            fillColor(linia, coloana, colorflag);
                            playersLines[activePlayer]++;
                            addNewScore();
                        }
                    }


                    if(activePlayer==2)
                        {
                            time_t t;
                            time(&t);
                            int dft = 5-difftime(t, timePlayers[2]);
                            if(dft>=0 && playersLines[2]>=1)
                            playersScore[2]+=dft;
                            activePlayer=1;
                            time(&timePlayers[1]);

                        }
                    else
                        {
                            time_t t;
                            time(&t);
                            int dft = 5-difftime(t, timePlayers[1]);
                            if(dft>=0 && playersLines[1]>=1)
                            playersScore[1]+=dft;
                            activePlayer=2;
                            time(&timePlayers[2]);

                        }
                        drawPlayers();
                        nrc++;
                        if(nrc+8==64 || (nrc>=10 && earlyEnd()==0))
                            return 1;
                }
            }
        }
    }

    return 0;
}

int earlyEnd()
{
    int steps1=0, steps2=0, found1, found2;

    for(int i=1;i<=8;i++)
        for(int j=1;j<=8;j++)
            x[i][j]=TablaDeJoc[i][j];

    int ok=0;
    for(int i=1;i<=8;i++)
        for(int j=1;j<=8;j++)
            if(x[i][j]==0)
            {
                filFoundColor=0;
                filEnd(i, j, 1);
                found1=filFoundColor;
                steps1 = max(filVerif(i, j), steps1);
            }

    for(int i=1;i<=8;i++)
        for(int j=1;j<=8;j++)
            x[i][j]=TablaDeJoc[i][j];

     for(int i=1;i<=8;i++)
        for(int j=1;j<=8;j++)
            if(x[i][j]==0)
               {
                   filFoundColor=0;
                   filEnd(i, j, 2);
                   found2=filFoundColor;
                   steps2 = max(filVerif(i, j), steps2);
               }

    if((steps1>=8 && steps2>=8) && (found2==1 ||  found1==1))
        ok=1;

    return ok;

}


int PvP ()
{
   //readimagefile("./images/gameBack.jpg", 0, 0, getwindowwidth(), getwindowheight());
    settextjustify(CENTER_TEXT, CENTER_TEXT);
    settextstyle(GOTHIC_FONT, HORIZ_DIR, 4);
    setcolor(BLACK);

    outtextxy(getwindowwidth()/2, getwindowheight()/10-20, displayText[displayLanguage][35]);

    settextstyle(GOTHIC_FONT, HORIZ_DIR, 1);
    outtextxy(getwindowwidth()/2, getwindowheight()-60, displayText[displayLanguage][33]);
    outtextxy(getwindowwidth()/2, getwindowheight()-40, displayText[displayLanguage][34]);
    activePlayer=1;
    playersScore[1]=playersScore[2]=0;
    nrc=0;

    for(int i=0;i<20;i++)
        VculoriOC[i]=0;

    for(int i=0;i<10;i++)
        playersLines[i]=0;

    gameOn=1;

    drawPlayers();
    time(&timePlayers[1]);

    while(gameOn==1)
    {
     //cout<<punerePiesa();
        if(punerePiesa()==1)
            return 1;
        if(kbhit())
        {
            char c;
            c = getch();
            if(c==27) return -1;
        }
    }
}

void desen()
{


    int k=0;
    int i,j;
    numar=8;
    width=std::min(getwindowheight(),getwindowwidth())*3/4;
    latura=width/numar;
    height=width;
    sus=(getwindowheight()-width)/2;
    stanga=(getwindowwidth()- width)/2;
   // setbkcolor(FUNDAL);

    clearviewport();
    setcolor(BLUE);

    for (i=1; i<=numar; i++)
        for (j=1; j<=numar; j++)
        {
            rectangle(stanga+latura*(i-1),sus+latura*(j-1),stanga+latura*i,sus+latura*j);
            k++;
            CentreCasute[k].x=float((stanga+latura*(i-1)+stanga+latura*i)/2);
            CentreCasute[k].y=float((sus+latura*(j-1)+sus+latura*j)/2);
        }
    PunereCercuriRandom();

}
